    <?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('../vue/utilisateur/liste.php',["utilisateurs" => $utilisateurs]);
    }

    public static function afficherDetail() : void {
        $login=$_GET["login"];
        if (empty($login)) {
            ControleurUtilisateur::afficherVue('../vue/utilisateur/erreur.php');

        }
        else {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login); //appel au modèle pour gérer la BD

            if(empty($utilisateur)) {
                ControleurUtilisateur::afficherVue('../vue/utilisateur/erreur.php');
            }else {
                ControleurUtilisateur::afficherVue('../vue/utilisateur/detail.php', ["utilisateur" => $utilisateur]);
            }
        }


    }



    public static function afficherFormulaireCreation() : void{
        ControleurUtilisateur::afficherVue('../vue/utilisateur/formulaireCreation.php');
    }

    public static function creerDepuisFormulaire() : void{


        foreach ($_GET as $key => $value) {
            $tab[]=$value;
        }
        $utilisateur = new ModeleUtilisateur($tab[3], $tab[2], $tab[1]);
        $utilisateur->ajouter();


        ControleurUtilisateur::afficherListe();


    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }




}
?>