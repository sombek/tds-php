<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\ModeleUtilisateur;

require_once __DIR__ . '/../Modele/ModeleUtilisateur.php'; // chargement du modèle
class ControleurUtilisateur
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('../vue/vueGenerale.php', ["utilisateurs"=>$utilisateurs,"titre" => "Liste des utilisateurs","cheminCorpsVue"=>"utilisateur/liste.php"]);
    }

    public static function afficherDetail(): void
    {
        $login = $_GET["login"];
        if (empty($login)) {
            ControleurUtilisateur::afficherVue('../vue/utilisateur/erreur.php');

        } else {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login); //appel au modèle pour gérer la BD

            if (empty($utilisateur)) {
                ControleurUtilisateur::afficherVue('../vue/vueGenerale.php', ["titre" => "Liste des utilisateurs","cheminCorpsVue"=>"utilisateur/erreur.php"]);
            } else {
                ControleurUtilisateur::afficherVue('../vue/vueGenerale.php', ["utilisateur"=>$utilisateur,"titre" => "Liste des utilisateurs","cheminCorpsVue"=>"utilisateur/detail.php"]);
            }
        }


    }


    public static function afficherFormulaireCreation(): void
    {
        ControleurUtilisateur::afficherVue('../vue/vueGenerale.php', ["titre" => "Liste des utilisateurs","cheminCorpsVue"=>"utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {


        foreach ($_GET as $key => $value) {
            $tab[] = $value;
        }
        $utilisateur = new ModeleUtilisateur($tab[3], $tab[1], $tab[2]);
        $utilisateur->ajouter();
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();

        ControleurUtilisateur::afficherVue('../vue/vueGenerale.php', ["utilisateurs"=>$utilisateurs,"titre" => "Liste des utilisateurs","cheminCorpsVue"=>"utilisateur/utilisateurCree.php"]);;


    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }


}

?>