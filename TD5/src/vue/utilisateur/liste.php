<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Liste des utilisateurs</title>
    </head>
    <body>
        <?php
        /** @var ModeleUtilisateur[] $utilisateurs */

        use App\Covoiturage\Modele\ModeleUtilisateur;

        foreach ($utilisateurs as $utilisateur){
            $loginHTML =htmlspecialchars( $utilisateur->getLogin());
            $loginURL=rawurlencode( ($utilisateur->getLogin()));
            echo '<p>L\'utilisateur de login <a href="http://localhost/tds-php/TD5/web/controleurFrontal.php?action=afficherDetail&login='. $loginURL.'">'. $loginHTML. '</a>.</p>';
        }
        echo '<p> <a href="http://localhost/tds-php/TD5/web/controleurFrontal.php?action=afficherFormulaireCreation"> Creer un utilisateur</a></p>';
        ?>
    </body>
</html>