<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Detail de l'utilisateur</title>
    </head>
    <body>
        <?php
        /** @var ModeleUtilisateur[] $utilisateur */

        use App\Covoiturage\Modele\ModeleUtilisateur;

        echo '<p> Utilisateur de login ' . htmlspecialchars($utilisateur->getLogin())  . " dont l'identité est " . htmlspecialchars( $utilisateur->getnom() ).' '. htmlspecialchars( $utilisateur->getPrenom()). '.</p>';
        ?>
    </body>
</html>