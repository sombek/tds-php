<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

// chargement du modèle
class ControleurUtilisateur
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateurs"=>$utilisateurs,"titre" => "Liste des utilisateurs","cheminCorpsVue"=>"utilisateur/liste.php"]);
    }


    public static function afficherDetail(): void
    {
        $login = $_GET["login"];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login); //appel au modèle pour gérer la BD
        if (empty($login)) {

            ControleurUtilisateur::afficherErreur("Il n'y a aucun login");

        } else {



            if (empty($utilisateur)) {

                ControleurUtilisateur::afficherErreur("L'utilisateur n'existe pas");
            } else {
                ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateur"=>$utilisateur,"titre" => "Détail de l'utilisateur","cheminCorpsVue"=>"utilisateur/detail.php"]);
            }
        }


    }


    public static function afficherFormulaireCreation(): void
    {
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "Formulaire de création d'utilisateur","cheminCorpsVue"=>"utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {


        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository)->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository)->recuperer();

        ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateurs"=>$utilisateurs,"titre" => "Utilisateur créé","cheminCorpsVue"=>"utilisateur/utilisateurCree.php"]);;


    }

    public static function afficherErreur(string $messageErreur=" "){

        ControleurUtilisateur::afficherVue('vueGenerale.php',["titre"=>"Erreur","messageErreur"=>$messageErreur,"cheminCorpsVue"=>"utilisateur/erreur.php"]);
    }



    public static function supprimer(){

        $login=$_GET["login"];


        if (empty($login)) {
            ControleurUtilisateur::afficherErreur("Cet utilisateur n'existe pas ");
        }
        else{

            (new UtilisateurRepository())->supprimer($login);
            $utilisateurs = (new UtilisateurRepository())->recuperer();

            ControleurUtilisateur::afficherVue("vueGenerale.php",["titre"=>"Utilisateur supprime","utilisateurs"=>$utilisateurs,"cheminCorpsVue"=>"utilisateur/utilisateurSupprime.php"]);

        }

    }


    public static function afficherFormulaireMiseAJour(){
        $login=$_GET["login"];
        $utilisateur=(new UtilisateurRepository())->recupererParClePrimaire($login);
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateur"=>$utilisateur,"titre" => "Mise à jour d'un utilisateur","cheminCorpsVue"=>"utilisateur/formulaireMiseAJour.php"]);

    }

    public static function mettreAJour(){
        $utilisateur=new Utilisateur($_GET["login"],$_GET["nom"],$_GET["prenom"]);
        (new UtilisateurRepository)->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "login" => $utilisateur->getLogin(), "titre" => "Mise à jour  d'utilisateur effectuée", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
    }


    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    /**
     * @param array $tableauDonneesFormulaire
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        foreach ($_GET as $key => $value) {
            $tab[] = $value;
        }
        $utilisateur = new Utilisateur($tab[2], $tab[3], $tab[4]);
        return $utilisateur;
    }


}

?>