<?php
namespace App\Covoiturage\Modele\DataObject;
use App\Covoiturage\Connexion\ConnexionBaseDeDonnees;
use Trajet;



class Utilisateur extends AbstractDataObject
{

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    // un setter
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    // un setter
    public function setLogin(string $login)
    {
        $this->login = substr($login, 0, 64);
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
    )
    {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    /*public function __toString() :string {
        return "L'utilisateur $this->nom $this->prenom, de login $this->login";
    }*/


    



//----------------------------------------------------------------------- PAS SÛR

    
//---------------------------------------------------------------------------------------
}

?>