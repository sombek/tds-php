<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\HTTP\Cookie;

// chargement du modèle
class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ["utilisateurs"=>$utilisateurs,"titre" => "Liste des utilisateurs","cheminCorpsVue"=>"utilisateur/liste.php"]);
    }


    public static function afficherDetail(): void
    {
        $login = $_GET["login"];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login); //appel au modèle pour gérer la BD
        if (empty($login)) {

            self::afficherErreur("Il n'y a aucun login");

        } else {



            if (empty($utilisateur)) {

                self::afficherErreur("L'utilisateur n'existe pas");
            } else {
                self::afficherVue('vueGenerale.php', ["utilisateur"=>$utilisateur,"titre" => "Détail de l'utilisateur","cheminCorpsVue"=>"utilisateur/detail.php"]);
            }
        }


    }


    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire de création d'utilisateur","cheminCorpsVue"=>"utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {


        $utilisateur = self::construireDepuisFormulaire($_GET);

        if($_GET["mdp"]!=$_GET["mdp2"]){
            self::afficherErreur("Mot de passe distincts");
        }else {
            (new UtilisateurRepository)->ajouter($utilisateur);
            $utilisateurs = (new UtilisateurRepository)->recuperer();

            self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "titre" => "Utilisateur créé", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);;
        }

    }

    public static function afficherErreur(string $messageErreur=" "){

        self::afficherVue('vueGenerale.php',["titre"=>"Erreur","messageErreur"=>$messageErreur,"cheminCorpsVue"=>"utilisateur/erreur.php"]);
    }



    public static function supprimer(){

        $login=$_GET["login"];


        if (!ConnexionUtilisateur::estUtilisateur($login)) {
            self::afficherErreur("Vous n'avez pas les droits pour supprimer ce compte ");
        }
        else{
            foreach ((new UtilisateurRepository())->recuperer() as $user){
                if($user->getLogin()==$login){
                    (new UtilisateurRepository())->supprimer($login);
                    ConnexionUtilisateur::deconnecter();
                    $utilisateurs = (new UtilisateurRepository())->recuperer();

                    self::afficherVue("vueGenerale.php",["titre"=>"Utilisateur supprime","utilisateurs"=>$utilisateurs,"cheminCorpsVue"=>"utilisateur/utilisateurSupprime.php"]);

                }
            }
        }

    }


    public static function afficherFormulaireMiseAJour(){

        $login=$_GET["login"];
        $utilisateur=(new UtilisateurRepository())->recupererParClePrimaire($login);
        if((new ConnexionUtilisateur())::estUtilisateur($login)|| (ConnexionUtilisateur::estAdministrateur()&&(new UtilisateurRepository())->recupererParClePrimaire($login))!=null) {
            self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Mise à jour d'un utilisateur", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"]);
        }
        else if(!ConnexionUtilisateur::estUtilisateur($login)||!ConnexionUtilisateur::estAdministrateur()){
            self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté");
        }else {
                self::afficherErreur("Login inconnu");

        }

    }

    public static function mettreAJour()
    {
        $estadmin=ConnexionUtilisateur::estAdministrateur();

        $nouveauMdp = MotDePasse::hacher($_GET["mdp1"]);
        $login=$_GET["login"];
         $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        $utilisateur->setPrenom($_GET["prenom"]);
        $utilisateur->setNom($_GET["prenom"]);
        if($estadmin){
            $utilisateur->setEstAdmin($_GET["estAdmin"]);
        }
        $utilisateur->setMdpHache(MotDePasse::hacher($_GET["mdp1"]));





        foreach ($_GET as $valeur) {
            if (is_null($valeur)) {
                self::afficherErreur("Un ou plusieurs champs manquant ");
            }
        }
        if($estadmin &&(new UtilisateurRepository())->recupererParClePrimaire($login)!=null){
            if ($_GET["mdp1"] != $_GET["mdp2"]) {
                self::afficherErreur("Les mots de passes ne correspondent pas ");
            }else{
                (new UtilisateurRepository)->mettreAJour($utilisateur);
                $utilisateurs = (new UtilisateurRepository())->recuperer();
                self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "login" => $utilisateur->getLogin(), "titre" => "Mise à jour  d'utilisateur effectuée", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);

            }
        }else if((new ConnexionUtilisateur())::estUtilisateur($login)) {
            if (!MotDePasse::verifier($_GET["ancienMdp"], $utilisateur->getMdpHache())) {
                self::afficherErreur("Ancien mot de passe incorrect");
            } else if ($_GET["mdp1"] != $_GET["mdp2"]) {
                self::afficherErreur("Les mots de passes ne correspondent pas ");
            } else {
                (new UtilisateurRepository)->mettreAJour($utilisateur);
                $utilisateurs = (new UtilisateurRepository())->recuperer();
                self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "login" => $utilisateur->getLogin(), "titre" => "Mise à jour  d'utilisateur effectuée", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
            }
        }else{
            if($estadmin){
                self::afficherErreur("Login inconnu");
            }else{
                self::afficherErreur("La mise à jour n'est possible que pour l'utilisateur connecté");
            }

        }

    }






    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        foreach ($tableauDonneesFormulaire as $value) {
            $tab[] = $value;
        }
        $nonce=MotDePasse::genererChaineAleatoire();


        $mdp=MotDePasse::hacher($tableauDonneesFormulaire["mdp"]);
        if(ConnexionUtilisateur::estAdministrateur()){
            $utilisateur = new Utilisateur($tableauDonneesFormulaire["login"], $tableauDonneesFormulaire["nom"], $tableauDonneesFormulaire["prenom"],"",$_GET["email"],$nonce,$mdp,isset($_GET["estAdmin"]));

        }else {
            $utilisateur = new Utilisateur($tableauDonneesFormulaire["login"], $tableauDonneesFormulaire["nom"], $tableauDonneesFormulaire["prenom"],"",$_GET["email"],$nonce,$mdp,false);

        }
        VerificationEmail::envoiEmailValidation($utilisateur);
        return $utilisateur;
    }


    public static function afficherFormulaireConnexion(){
        self::afficherVue('vueGenerale.php', ["titre" => "Connexion","cheminCorpsVue"=>"utilisateur/formulaireConnexion.php"]);
    }


    public static function connecter(){
        if(!isset($_GET["login"])|| !isset($_GET["mdp"])){
            self::afficherErreur("Login et/ou mot de passe manquant");
        }else{
            $login=$_GET["login"];
            $mdp=$_GET["mdp"];
            $utilisateur=(new UtilisateurRepository())->recupererParClePrimaire($login);
            $mdpHache=$utilisateur->getMdpHache();
            if(!MotDePasse::verifier($mdp,$mdpHache)){
                self::afficherErreur("Login et/ou mot de passe incorrect");
            }else{
                ConnexionUtilisateur::connecter($login);
                self::afficherVue('vueGenerale.php', ["utilisateur"=>$utilisateur,"titre" => "Connecté","cheminCorpsVue"=>"utilisateur/utilisateurConnecte.php"]);

            }

        }
    }


    public static function deconnecter(){
        ConnexionUtilisateur::deconnecter();
        $utilisateurs=(new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php",["titre"=>"Déconnecté","utilisateurs"=>$utilisateurs,"cheminCorpsVue"=>"utilisateur/utilisateurDeconnecte.php"]);
    }



    public static function validerEmail(){
        $login=$_GET["login"];
        $nonce=$_GET["nonce"];
        if(!isset($nonce)||!isset($login)){
            self::afficherErreur("Information manquante our la vérification");
        }
        if(!VerificationEmail::traiterEmailValidation($login,$nonce)){
            self::afficherErreur("Problème lors de la vérification");
        }
    }




















    /*public static function deposerCookie(){

         Cookie::enregistrer($_GET["cle"],$_GET["valeur"],$_GET["dureeExpiration"]);
    }

    public static function lireCookie($cle){
        return Cookie::lire($cle);
    }*/




}

