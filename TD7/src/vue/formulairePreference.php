<form method="get" action="controleurFrontal.php">

    <input type="hidden" name="action" value="enregistrerPreference"/>
    <fieldset>
        <legend>Controleur par défault :</legend>

            <?php

            use App\Covoiturage\Lib\PreferenceControleur;

            if (PreferenceControleur::existe()){
               if (PreferenceControleur::lire()=='utilisateur'){
                   echo'<p class="InputAddOn">
                            <input type="radio" id="utilisateurId"  checked name="controleur_defaut" value="utilisateur">
                            <label for="utilisateurId">Utilisateur</label>
                        </p>';
                   echo'<p class="InputAddOn">
                        <input type="radio" id="trajetId" name="controleur_defaut" value="trajet">
                        <label for="trajetId">Trajet</label>
                    </p>';
               }
               else {
                   echo'<p class="InputAddOn">
                            <input type="radio" id="utilisateurId"   name="controleur_defaut" value="utilisateur">
                            <label for="utilisateurId">Utilisateur</label>
                        </p>';
                   echo'<p class="InputAddOn">
                            <input type="radio" id="trajetId" checked name="controleur_defaut" value="trajet">
                            <label for="trajetId">Trajet</label>
                        </p>';
               }
            }else {
                echo'<p class="InputAddOn">
                            <input type="radio" id="utilisateurId"   name="controleur_defaut" value="utilisateur">
                            <label for="utilisateurId">Utilisateur</label>
                        </p>';
                echo'<p class="InputAddOn">
                        <input type="radio" id="trajetId" name="controleur_defaut" value="trajet">
                        <label for="trajetId">Trajet</label>
                    </p>';
            }
            ?>



        <p class="InputAddOn">
        <input class="InputAddOn-field" type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
