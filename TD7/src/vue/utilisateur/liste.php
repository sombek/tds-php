<?php

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;

/** @var Utilisateur[] $utilisateurs */

foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode(($utilisateur->getLogin()));
    echo '<p> Utilisateur de login <a href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=' . $loginURL . '">' . $loginHTML . '</a>&ensp;';
    if(ConnexionUtilisateur::estAdministrateur()) {
        echo '(<a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . $loginURL . '">Modifier</a>)</p>';

    }
}
echo '<p> <a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation"> Creer un utilisateur</a></p>';
