<?php

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\GenerateurAvis\Modele\DataObject\Utilisateur;
/** @var Utilisateur $utilisateur */

?>
<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="controleur" value="utilisateur"/>
    <input type="hidden" name="action" value="mettreAJour"/>

    <fieldset>
        <legend>Modifiaction d'utilisateur :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text"  name="login" id="login_id" value="<?= htmlspecialchars($utilisateur->getLogin())?>" readonly>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
            <input class="InputAddOn-field" type="text"  name="nom" id="nom_id" value="<?= htmlspecialchars($utilisateur->getNom())?>" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prénom&#42;</label>
            <input class="InputAddOn-field" type="text" name="prenom" id="prenom_id" value="<?= htmlspecialchars($utilisateur->getPrenom())?>" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="ancienMdp_id">Ancien mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" name="ancienMdp" id="ancienMdp_id"  >
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id1">Nouveau mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" name="mdp1" id="mdp_id1"  required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id2">Verification mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" name="mdp2" id="mdp_id2"  required>
        </p>

        <?php
            if(ConnexionUtilisateur::estAdministrateur()) {
                if ($utilisateur->isEstAdmin()) {
                    echo '<p class="InputAddOn">
                            <label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
                            <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id" checked>
                        </p>';
                } else {
                    echo '<p class="InputAddOn">
                            <label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
                            <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id">
                        </p>';
                }
            }
        ?>





        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Envoyer" />
        </p>

    </fieldset>
</form>