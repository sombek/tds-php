
        <?php
        /** @var \App\Covoiturage\Modele\DataObject\Utilisateur[] $utilisateur */

        use App\Covoiturage\Lib\ConnexionUtilisateur;
        use App\Covoiturage\Modele\DataObject\Utilisateur;
        use App\Covoiturage\Modele\HTTP\Session;

        $loginHTML = htmlspecialchars($utilisateur->getLogin());
        $loginURL = rawurlencode(($utilisateur->getLogin()));
        echo '<p> Utilisateur de login ' . htmlspecialchars($utilisateur->getLogin())  . " dont l'identité est " . htmlspecialchars( $utilisateur->getnom() ).' '. htmlspecialchars( $utilisateur->getPrenom()). '.</p>';
        if(ConnexionUtilisateur::estUtilisateur($loginURL)){
                echo '<p>(<a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . $loginURL . '">Modifier </a>&ensp;<a href="controleurFrontal.php?controleur=utilisateur&action=supprimer&login=' . $loginURL . '">Supprimer ?</a>)';
        }

        ?>
