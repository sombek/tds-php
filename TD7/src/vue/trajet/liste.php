<?php

use App\Covoiturage\Modele\DataObject\Trajet;

/** @var Trajet[] $trajets */

foreach ($trajets as $trajet) {
    $idHTML = htmlspecialchars($trajet->getId());
    $idURL = rawurlencode(($trajet->getId()));
    echo '<p> Trajet d\'id <a href="controleurFrontal.php?controleur=trajet&action=afficherDetail&id=' . $idURL . '">' . $idHTML . '</a> &ensp;'.  '<a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&id=' . $idURL . '">Modifier </a>&ensp; <a href="controleurFrontal.php?controleur=trajet&action=supprimer&id=' . $idURL . '">Supprimer ?</a> </p>';
}
echo '<p> <a href="http://localhost/tds-php/TD6/web/controleurFrontal.php?controleur=trajet&action=afficherFormulaireCreation"> Creer un trajet</a></p>';
?>
