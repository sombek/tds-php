<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie
{






    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void{

        $valeurS=serialize($valeur);

        if($dureeExpiration==null){
            setcookie($cle, $valeurS);
        }else {
            setcookie($cle, $valeurS,  time()+$dureeExpiration);
        }
    }

    public static function lire(string $cle): mixed{
        return unserialize( $_COOKIE[$cle]);
    }

    public static function contient($cle) : bool{
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle) : void{
        if(Cookie::contient($cle)){
            setcookie ($cle, "", 1);
            unset($_COOKIE[$cle]);
        }


    }

}