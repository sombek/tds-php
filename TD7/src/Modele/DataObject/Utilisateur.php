<?php
namespace App\Covoiturage\Modele\DataObject;
use App\Covoiturage\Connexion\ConnexionBaseDeDonnees;
use Cassandra\Tinyint;
use Trajet;



class Utilisateur extends AbstractDataObject
{

    private string $login;
    private string $nom;
    private string $prenom;
    private string $email;
    private string $emailAValider;
    private string $nonce;
    private string $mdpHache;

    private bool $estAdmin;



    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }


    public function getPrenom(): string
    {
        return $this->prenom;
    }

    // un setter
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    // un setter
    public function setLogin(string $login)
    {
        $this->login = substr($login, 0, 64);
    }

    public function getEmailAValider(): string
    {
        return $this->emailAValider;
    }

    public function setEmailAValider(string $emailAValider): void
    {
        $this->emailAValider = $emailAValider;
    }

    public function getNonce(): string
    {
        return $this->nonce;
    }

    public function setNonce(string $nonce): void
    {
        $this->nonce = $nonce;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getMdpHache(): string
    {
        return $this->mdpHache;
    }

    public function setMdpHache(string $mdpHache): void
    {
        $this->mdpHache = $mdpHache;
    }

    public function isEstAdmin(): bool
    {
        return $this->estAdmin;
    }

    public function setEstAdmin(bool $estAdmin): void
    {
        $this->estAdmin = $estAdmin;
    }






    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom,
        string $email,
        string $emailAValider,
        string $nonce,
        string $mdpHache,
        bool $estAdmin
    )
    {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->email = $email;
        $this->emailAValider = $emailAValider;
        $this->nonce = $nonce;
        $this->mdpHache = $mdpHache;
        $this->estAdmin = $estAdmin;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    /*public function __toString() :string {
        return "L'utilisateur $this->nom $this->prenom, de login $this->login";
    }*/


    



//----------------------------------------------------------------------- PAS SÛR

    
//---------------------------------------------------------------------------------------
}

?>