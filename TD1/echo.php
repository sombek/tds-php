<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          $texte = "hello world !<br>";

          // On écrit le contenu de la variable 'texte' dans la page Web
          echo $texte;
          $nom="Mwa";
          $prenom="Mem";
          $login="Aiklyn";
          echo "Utilisateur : $nom $prenom de login $login<br>";

          $utilisateur=['nom'=>$nom,'prenom'=>$prenom,'login'=>$login];
          echo "Utilisateur $utilisateur[nom] $utilisateur[prenom] de login $utilisateur[login] <br>";

          $utilisateur2=['nom'=>"Goldé",'prenom'=>"Pol",'login'=>"Miracle"];
          $utilisateur3=['nom'=>"Tolko",'prenom'=>"Ome",'login'=>"Igritt"];
          $utilisateur4=['nom'=>"Filou",'prenom'=>"Lucaca",'login'=>"Shin"];
          $utilisateurs=[$utilisateur2,$utilisateur3,$utilisateur4];
          if(empty($utilisateurs)){
              echo 'Il n\'y a aucun utilisateur';
          }
          else{
              foreach ($utilisateurs as $i =>$utilisateur) {
                echo "Utilisateur $utilisateur[nom] $utilisateur[prenom] de login $utilisateur[login] <br>";
              }
          }

        ?>
    </body>
</html> 